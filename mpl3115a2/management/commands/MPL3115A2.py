#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from smbus import SMBus
import time

__all__ = ["pressure_temperature"]

# I2C Constants
ADDR = 0x60
CTRL_REG1 = 0x26
PT_DATA_CFG = 0x13


def pressure_temperature():
    # get the bus
    bus = SMBus(1)

    # check that the device is connected
    who_am_i = bus.read_byte_data(ADDR, 0x0C)
    if who_am_i != 0xc4:
        raise Exception("MPL3115A2 device not connected")

    # Set oversample rate to 128
    setting = bus.read_byte_data(ADDR, CTRL_REG1)
    newSetting = setting | 0x38
    bus.write_byte_data(ADDR, CTRL_REG1, newSetting)

    # Enable event flags
    bus.write_byte_data(ADDR, PT_DATA_CFG, 0x07)

    # Toggel One Shot
    setting = bus.read_byte_data(ADDR, CTRL_REG1)
    if (setting & 0x02) == 0:
        bus.write_byte_data(ADDR, CTRL_REG1, (setting | 0x02))

    # Read sensor data
    print("Waiting for data...")
    status = bus.read_byte_data(ADDR, 0x00)
    while (status & 0x08) == 0:
        status = bus.read_byte_data(ADDR, 0x00)
        time.sleep(0.5)

    print("Reading sensor data...")
    p_data = bus.read_i2c_block_data(ADDR, 0x01, 3)
    t_data = bus.read_i2c_block_data(ADDR, 0x04, 2)
    status = bus.read_byte_data(ADDR, 0x00)
    print("status: "+bin(status))

    p_msb = p_data[0]
    p_csb = p_data[1]
    p_lsb = p_data[2]
    t_msb = t_data[0]
    t_lsb = t_data[1]

    pressure = (p_msb << 10) | (p_csb << 2) | (p_lsb >> 6)
    p_decimal = ((p_lsb & 0x30) >> 4)/4.0

    celsius = t_msb + (t_lsb >> 4)/16.0

    return pressure + p_decimal, celsius


# vim: set tw=79 :
