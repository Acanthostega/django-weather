#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging
import os

from django.core.management.base import BaseCommand
from django.core.management.base import CommandError
from mpl3115a2.models import Weather
from .MPL3115A2 import pressure_temperature

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Update pressure and temperature from the MPL3315A2 chip"

    def handle(self, *args, **options):
        """
        Load the informations from the chipset to the database.
        """
        # get the informations from the device
        try:
            # first execute a command to indicate repeated start for I2C in
            # raspberry
            os.system(
                "echo -n 1 > /sys/module/i2c_bcm2708/parameters/combined"
            )

            # get pressure and temperature
            pressure, temperature = pressure_temperature()

            # create the object
            data = Weather(
                pressure=pressure,
                temperature=temperature,
            )

            # save the data into the database
            data.save()

        except Exception as e:
            logger.error(e)
            raise CommandError(
                "Can't execute the pressure, temperature command"
            )


# vim: set tw=79 :
