from django.db import models


class Weather(models.Model):
    """
    Representation of the temporal informations about the weather.
    """
    # the time at which the measure is done
    date = models.DateTimeField(auto_now_add=True)

    # the pressure
    pressure = models.FloatField()

    # the temperature
    temperature = models.FloatField()


# Create your models here.
