#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from rest_framework import serializers
from .models import Weather


class WeatherSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Weather
        fields = ("date", "pressure", "temperature")


# vim: set tw=79 :
