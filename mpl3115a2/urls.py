#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.conf.urls import url, include
from rest_framework import routers
from .views import WeatherViewSet

router = routers.DefaultRouter()
router.register(r"measures", WeatherViewSet)

urlpatterns = [
    url(r"^", include(router.urls)),
]


# vim: set tw=79 :
