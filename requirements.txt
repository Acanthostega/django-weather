cffi==1.2.1
Django==1.8.4
djangorestframework==3.2.3
pycparser==2.14
smbus-cffi==0.5.1
wheel==0.24.0
